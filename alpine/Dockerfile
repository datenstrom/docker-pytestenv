FROM alpine:latest

# use pyenv understandable version
#ARG PYTHON_VERSION
ENV HOME=/root \
    PROFILE=$HOME/.bashrc \
    PYENV_ROOT=/root/.pyenv \
    PATH=$PYENV_ROOT/bin:$PATH
#    PYTHON_VERSION=${PYTHON_VERSION:-3.6.0}

# Install needed packages. Notes:
#   * linux-headers: commonly needed, and an unusual package name from Alpine.
#   * build-base: used so we include the basic development packages (gcc)
#   * bash: so we can access /bin/bash
#   * git: to ease up clones of repos
#   * ca-certificates: for SSL verification during Pip and easy_install
#   * python3: the binaries themselves and pip3
#   * curl: for fetching install script
#   * ca-certificates: for curl to fetching install script
ENV PACKAGES="\
    bash \
    build-base \
    patch \
    ca-certificates \
    git \
    bzip2-dev \
    linux-headers \
    ncurses-dev \
    openssl \
    openssl-dev \
    readline-dev \
    python3 \
    sqlite-dev"

RUN echo \
    && apk add --no-cache --update $PACKAGES\
    && update-ca-certificates \
    && rm -rf /var/cache/apk/*

SHELL ["/bin/bash", "-c"]

RUN echo \
    && git clone --depth 1 https://github.com/yyuu/pyenv.git ~/.pyenv \
    && rm -rfv /root/.pyenv/.git \
    && export PYENV_ROOT="/root/.pyenv" \
    && export PATH="$PYENV_ROOT/bin:$PATH" \
    && export CFLAGS='-O2' \
    && eval "$(pyenv init -)" \
    && eval "$(pyenv virtualenv-init -)" \
    && source /root/.pyenv/completions/pyenv.bash \
#    && pyenv install --skip-existing --verbose 2.7 \
#    && pyenv install --skip-existing --verbose 3.1 \
#    && pyenv install --skip-existing --verbose 3.2 \
    && pyenv install --skip-existing --verbose 3.3.6 \
    && pyenv install --skip-existing --verbose 3.4.6 \
    && pyenv install --skip-existing --verbose 3.5.3 \
    && pyenv install --skip-existing --verbose 3.6.1 \
    && pip install --upgrade pip \
    && pyenv rehash

# Setup ENV on start
COPY init.sh /init.sh
CMD bash -C '/init.sh';'bash'
